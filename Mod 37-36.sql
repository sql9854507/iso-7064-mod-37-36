
DECLARE @calculationString VARCHAR(1000) = '123456789'

;WITH cte AS
(
	SELECT 
		36 as mod,
		SUBSTRING(@calculationString, 1, 1) c, 
		1 AS ind,
		36 as cNum
	UNION ALL 
	SELECT 
		mod,
		SUBSTRING(@calculationString, ind + 1, 1),
		ind + 1,
		CASE
			WHEN cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55) > mod THEN 
				CASE 
					WHEN 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55) - mod) > mod THEN 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55) - mod) - mod - 1
					ELSE 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55) - mod)
				END
			ELSE 
				CASE
					WHEN 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55)) > mod THEN 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55)) - mod - 1
					ELSE 2 * (cNum + IIF(ISNUMERIC(c) = 1, c, ASCII(c) - 55))
				END
		END
	FROM 
		cte 
	WHERE 
		LEN(@calculationString) + 1 > ind 
)

SELECT
	CASE
		WHEN cNum = 1 THEN '0'
		WHEN mod + 1 - cNum < 10 THEN CAST(mod + 1 - cNum AS varchar)
		ELSE CHAR(mod + 1 - cNum + 55)
	END
FROM 
	cte
WHERE
	ind = LEN(@calculationString) + 1
OPTION (maxrecursion 0)
